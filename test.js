var ooooooo = new require('./ooooooo')({database: 'test'}).connect();
var assert = require('assert');

var UserDefinition = function() {
	this.is_a_test = true;
	this.sayName = function() {
		console.log(this.username);
	};
	return this;
};

var UserTable = function() {
	this.whatAmI = function() {
		return 'I am a users table.';
	};
};

UserDefinition.table_name = 'users';

var User = ooooooo.registerTable({
	table_name: 'users',
	row_class: UserDefinition,
	table_class: UserTable
});

assert.notEqual(User.get, undefined);
assert.notEqual(User.whatAmI, undefined);

var phaseOne = function(callback) {
	User.truncate(function (err, result) {
		assert.equal(err, null);
		User.query('ALTER TABLE ?? AUTO_INCREMENT = 1', [User.table_name], function(err, result) {
			assert.equal(err, null);
			User.insert({username: 'derek'}, function(err, user) {
				assert.equal(err, null);
				assert.notEqual(user, undefined);		
				assert.strictEqual(user.username, 'derek');
				assert.notEqual(user.remove, undefined);
				User.get(1, function(err, user) {
					assert.equal(err, null);	
					assert.notEqual(user.sayName, undefined);
					user.username = 'arnold';
					user.save(function (err, row) {
						assert.equal(err, null);
						assert.equal(user.username, 'arnold');
						User.get(1, function(err, user) {
							assert.equal(err, null);
							assert.equal(user.username, 'arnold');
							callback();
						});
					});
				});
			});
		});
	});
};

var phaseTwo = function(callback) {
	User.truncate(function(err, result) {
		User.insert({username: 'derek'}, function() {
			User.insert({username: 'rua'}, function() {
				User.where({username: 'rua'}, function(_, ruas) {
					assert.equal(err, null);
					assert.notEqual(ruas, undefined);
					assert.notEqual(ruas.length, undefined);
					assert.equal(ruas[0].username, 'rua');
					callback();
				});
			});
		});
	});
};

var phaseThree = function(callback) {
	assert.notEqual(User.whatAmI, undefined);
	assert.equal(User.whatAmI(), 'I am a users table.');
	callback();
};

phaseOne(function() { 
	phaseTwo(function() {
		User.get(1, function(err, user) {
			assert.equal(err, null);
			assert.notEqual(user.sayName, undefined);
			User.query("SELECT 1 as `test`", function(err, rows) {
				assert.equal(err, null);
				assert.notEqual(rows.length, undefined);
				assert.notEqual(rows[0].test, undefined);
				assert.notEqual(rows[0].sayName, undefined);
				User.query("SELECT ?? FROM ?? LIMIT 1", ['username', 'users'], function(err, rows) {
					assert.equal(err, null);
					assert.notEqual(rows[0].username, undefined);
					assert.notEqual(rows[0].sayName, undefined);
					phaseThree(function() {
						ooooooo.end();
					});
				});
			});
		});
	});
});



