var mysql = require('mysql');

var Row = function(table) {

	this.table = table;
	this._columns = [];

	this.save = function(callback) {
		this.table.saveRow(this, callback);
	};

	this.remove = function(callback) {
		this.table.removeRow(this, callback);
	};

	this.data = function() {
		var data = {};
		for(var col in this._columns) {
			data[this._columns[col]] = this[this._columns[col]];
		}
		return data;
	};

	this._populate = function(data) {
		for(var col in data) {
			if (data.hasOwnProperty(col)) {
				this[col] = data[col];
				this._columns.push(col);
			}
		}
		delete this._populate;
	};

	this._bestow = function(row) {
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				row[key] = this[key];
			}
		}
		delete this._bestow;
		return row;
	}

	return this;
};

var Table = function(database, table_name, row_class, id_column_name) {
	this.database = database;
	this.row_class = row_class;
	this.table_name = table_name;
	this.id_column_name= id_column_name || 'id';

	this._atomicize = function(routine, callback) {
		this.database.connection.query('START TRANSACTION', (function(err, result) {
			var returner = (function(err, result) {
				var cbcb = (function(_, _) {
					callback(err, result);
				}).bind(this);

				if (!err) {
					this.database.connection.query('COMMIT', cbcb);
				} else {
					this.database.connection.query('ROLLBACK', cbcb);
				}
			}).bind(this);
			routine(returner);
		}).bind(this));
	};

	this.insert = function(data, callback) {
		var routine = (function(returner) {
			var query = "INSERT INTO ?? SET ?";
			this.database.connection.query(query, [this.table_name, data], (function(err, _) {
				this.database.connection.query("SELECT LAST_INSERT_ID() as `last`", (function(_, result2) {
					this.get(result2[0].last, function(_, row) {
						returner(err, row);
					});
				}).bind(this));
			}).bind(this));
		}).bind(this);

		this._atomicize(routine, callback);
	};

	this.removeRow = function(row, callback) {
		var routine = (function(returner) {
			this.database.connection.query(
				"DELETE FROM ?? WHERE ?? = ?", [this.table_name, this.id_column_name, row[this.id_column_name]],
				function(err, result) {
					returner(err, result && result.affectedRows === 1);
				});
		}).bind(this);

		this._atomicize(routine, callback);
	};

	this.saveRow = function(row, callback) {
		var routine = (function(returner) {
			var data = row.data();
			delete data[this.id_column_name];
			var updateData = [this.table_name, row.data(), this.id_column_name, row[this.id_column_name]];

			this.database.connection.query(
				"UPDATE ?? SET ? WHERE ?? = ?",
				updateData, function(err, result) {
					returner(err, result && result.affectedRows === 1);
				});
		}).bind(this);

		this._atomicize(routine, callback);
	};

	this._createWhereClause = function(params) {
		var args = [];
		for(var k in params) {
			if (params.hasOwnProperty(k)) {
				args.push('?? = ?');
			}
		}
		return args.join(' AND ');
	};

	this.all = function(callback) {
		this.database.connection.query("SELECT * FROM ??", [this.table_name], this._castRows(false, callback));
	};

	this.where = function(params, callback) {
		var args = [this.table_name];
		for(k in params) {
			if (params.hasOwnProperty(k)) {
				args.push(k);
				args.push(params[k]);
			}
		}
		this.database.connection.query(
			"SELECT * FROM ?? WHERE " + this._createWhereClause(params), 
			args, this._castRows(false, callback));
	};

	this.get = function(id, callback) {
		this.database.connection.query(
			"SELECT * FROM ?? WHERE ?? = ?",
			[this.table_name, this.id_column_name, id],
			this._castRows(true, callback));
	};

	this.query = function(query, params, callback) {
		if (callback === undefined) {
			callback = params;
			params = [];
		}
		this.database.connection.query(
			query, params, this._castRows(false, callback));
	};

	this.truncate = function(callback) {
		this.database.connection.query(
			"TRUNCATE TABLE ??", [this.table_name], callback);
	};

	this._castRows = function(justOne, callback) {
		var justOne = !!justOne;
		return (function (err, rows) {
			if (err) {
				callback(err, null);
				return;
			}
			var output = [];
			if (rows.length === undefined) {
				// this query has no rows to return;
				callback(err, rows);
				return;
			}

			for (var r in rows) {
				this.row_class.prototype = new Row(this);
				var row = new this.row_class();
				row._populate(rows[r]);

				output.push(row);
			}
			if (justOne) {
				callback(err, output[0]);
			} else {
				callback(err, output);
			}
		}).bind(this);
	};

	return this;
};

var ooooooo = function(options) {

	this.tables = {};

	this.registerTable = function(options) {

		// table_name, row_class, id_column_name
		
		if (options.table_name === undefined) {
			throw new 'You must define a table name.';
		}

		if (options.row_class === undefined) {
			options.row_class = function() {};
		}

		if (options.id_column_name === undefined) {
			options.id_column_name = 'id';
		}

		if (options.table_class === undefined) {
			options.table_class = function() {};
		}

		var table = new Table(this, options.table_name, options.row_class, options.id_column_name);
		options.table_class.prototype = table;
		this.tables[options.table_name] = new options.table_class();
		return this.tables[options.table_name];
	};

	this.options = options;
	this.options.connected = false;
	
	this.connect = (function(cb) {
		if (!this.options.connected) {
			this.connection = mysql.createConnection(options);
			this.connection.connect();
			this.options.connected = true;
		}
		return this;
	}).bind(this);

	this.end = function() {
		(this.connection.end).apply(this.connection);
	};

	return this;
};

module.exports = ooooooo;